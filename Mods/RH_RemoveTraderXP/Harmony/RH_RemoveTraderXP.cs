﻿using System;
using Harmony;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_RemoveTraderXP
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(Progression))]
    [HarmonyPatch("AddLevelExp")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(string), typeof(Progression.XPTypes), typeof(bool) })]
    class PatchProgressionAddLevelExp
    {
        static bool Prefix(Progression __instance, ref int _exp, ref string _cvarXPName, ref Progression.XPTypes _xpType, ref bool useBonus)
        {
            if (_cvarXPName == "_xpFromSelling")
                return false;

            return true;
        }
    }
}
