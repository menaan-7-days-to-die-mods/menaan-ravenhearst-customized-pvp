﻿using System;
using System.Linq;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Reflection.Emit;

public class RH_IncreaseTraderQuestLeveling
{
    public class RH_IncreaseTraderQuestLeveling_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    // Increase the number of quests needed to level a Tier from 5 to 10
    [HarmonyPatch(typeof(QuestJournal))]
    [HarmonyPatch("GetCurrentFactionTier")]
    [HarmonyPatch(new Type[] { typeof(byte), typeof(int), typeof(bool) })]
    public class PatchQuestJournalGetCurrentFactionTier
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_5)
                {
                    codes[i].opcode = OpCodes.Ldc_I4;
                    codes[i].operand = 10;
                }
            }

            return codes.AsEnumerable();
        }
    }
}
