﻿using System;
using Harmony;
using UnityEngine;
using System.Reflection;
using DMT;
using System.Collections.Generic;

public class RH_GameOptionZombieSight
{
    public class RH_GameOptionZombieSight_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityAlive))]
    [HarmonyPatch("CopyPropertiesFromEntityClass")]
    public class PatchEntityAliveCopyPropertiesFromEntityClass
    {
        static void Postfix(EntityAlive __instance)
        {
            var sightRange = 0;
            switch (GameStats.GetInt(EnumGameStats.SightRange))
            {
                case 0:
                    sightRange = 0;
                    break;
                case 1:
                    sightRange = 10;
                    break;
                case 2:
                    sightRange = 25;
                    break;
                case 3:
                    sightRange = 50;
                    break;
                case 4:
                    sightRange = 75;
                    break;
                case 5:
                    sightRange = 100;
                    break;
                case 6:
                    sightRange = 150;
                    break;
                case 7:
                    sightRange = 200;
                    break;
            }

            EntityClass entityClass = EntityClass.list[__instance.entityClass];
            __instance.sightRange += sightRange;
        }
    }
}

